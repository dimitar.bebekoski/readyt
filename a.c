#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>

namespace file {
void read(std::string &txt) {
  const std::filesystem::path &PATH = std::filesystem::path("/") / "home" /
                                      "dimitar" / ".cache" / "ytfzf" /
                                      "watch_hist";
  std::ifstream ifstream(PATH, std::ios::ate);

  if (!ifstream)
    return;

  const std::size_t SIZE = ifstream.tellg();

  txt.resize(SIZE);
  ifstream.seekg(0).read(txt.data(), SIZE);
}
} // namespace file

int main() {
  std::string txt;
  file::read(txt);
  std::cout << txt << std::endl;

  return 0;
}
